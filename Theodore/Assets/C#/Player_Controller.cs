﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{
    public Rigidbody Rb;
    public Animator Anim;
    [Header("玩家人物圖樣物件")]
    public SpriteRenderer Player = null;
    [Header("查看是否站起會撞到牆的碰撞器")]
    public SphereCollider Dis_Collider;
    [Header("上半身的碰撞器")]
    public BoxCollider Half_Collider;
    [Header("碰到的梯子")]
    public GameObject ladder = null;
    [Header("瓦爾特物件")]
    public GameObject WaRrTe;
    [Header("走路速度")]
    public float Walk_Speed = 15f;
    [Header("跳躍力")]
    public float JumpForce = 0;
    [Header("判斷人物是否在地上")]
    public bool Is_Ground = false;
    [Header("判斷人物是否被攻擊")]
    public bool IsHurt = false;
    [Header("判斷人物是否得維持蹲下")]
    public bool Keep_Squat = false;
    [Header("人物水平向量")]
    public float Horizontal_Move = 0;
    [Header("人物垂直向量")]
    public float Climb_Direction = 0;
    [Header("人物方向向量")]
    public float Face_Dircetion = 0;
    [Header("判斷跳躍按鍵是否被按下")]
    public bool JumpRressed = false;
    [Header("判斷是否在跳躍中")]
    public bool Is_Jumping = false;
    [Header("判斷是否能攀爬")]
    [SerializeField]
    public bool CanClimb, TopLadder, ButttonLadder = false;
    [Header("判斷是否能拿取瓦爾特")]
    public bool Take_WaRrTe = false;

    [Header("判斷瓦爾特是否冷卻中")]
    [SerializeField]
    public bool Is_Cold = false;
    [Header("決定瓦爾特冷卻時間")]
    [SerializeField]
    private float ColdTime = 0;
    [Header("瓦爾特剩餘冷卻時間")]
    [SerializeField]
    private float ColdingTime = 0;

    [Header("是否可以躲藏")]
    [SerializeField]
    private bool Can_Hide = false;
    [Header("判斷是否躲藏中")]
    [SerializeField]
    private bool Is_Hiding = false;
    [Header("抓取躲藏點的物件")]
    [SerializeField]
    private GameObject Hider;

    [Header("玩家眼睛位置")]
    [SerializeField]
    private Transform Eyes_pos;

    [Header("玩家的視線範圍")]
    [SerializeField]
    private float Eyes_Line = 0f;

    [Header("玩家所看見的物件")]
    [SerializeField]
    private GameObject Watching = null;

    [Header("判斷玩家是否在攀爬中")]
    [SerializeField]
    public bool Is_Climbing = false;

    [Header("檢測玩家是否在樓梯上")]
    [SerializeField]
    private bool Is_Stairs = false;

    [Header("玩家所碰觸的樓梯物件")]
    [SerializeField]
    private GameObject Stairs_GameObject = null;
    public enum Player_State { Walking,Climbing,End_Climb, Start_Climb,Done,Get_Hide };
    [Header("角色狀態")]
    [SerializeField]
    public Player_State State = Player_State.Walking;

    [Header("碰觸暗處躲藏點")]
    [SerializeField]
    private bool Touch_Dark_Point = false;

    [Header("碰觸存檔點")]
    [SerializeField]
    private bool Save_1, Save_2, Save_3 = false;

    [Header("存檔點位置")]
    [SerializeField]
    private Vector3 Save_Pos = new Vector3(0, 0, 0);
    [Header("撞到的敵人")]
    [SerializeField]
    private GameObject Enemy = null;

    [Header("調整攀爬時的位置")]
    [SerializeField]
    private float Rope_X = 0f;

    [Header("取走瓦爾特")]
    [SerializeField]
    private Sprite Taking = null;

    [Header("滑鼠物件")]
    [SerializeField]
    private Mouse_Cursor My_Cursor = null;

    [Header("各組動畫")]
    [SerializeField]
    private GameObject[] OverView;

    [Header("是否在無敵狀態")]
    [SerializeField]
    private bool Is_Invincible = false;

    [Header("無敵持續時間")]
    [SerializeField]
    private float Invincible_Time = 0f;

    [Header("剩餘無敵時間")]
    [SerializeField]
    private float Back_Normal_Time = 0;

    [Header("調整透明度")]
    [SerializeField]
    private Color Invincible_Alpha = new Color(0, 0, 0, 0.1f);

    [Header("顯示與不顯示")]
    [SerializeField]
    private int Flag = 1;

    [Header("無敵時閃爍的倍數")]
    [SerializeField]
    private float Invincible_Speed = 0f;

    [Header("拿走瓦爾特的動畫")]
    [SerializeField]
    private Plot Take_WRF = null;

    [Header("接觸瓦爾特的提示")]
    [SerializeField]
    private GameObject Touch_WRT = null;


    void Start()
    {

    }
    private void FixedUpdate()
    {
        switch (State)
        {
            case (Player_State.Walking):
                //一般走路
                Movement();
                //爬樓梯
                Climb_Stairs();
                break;
            case (Player_State.Climbing):
                Climbing();
                break;
            case (Player_State.End_Climb):
                break;
            case (Player_State.Start_Climb):
                break;
        }
        Switch_Anim();
    }
    private void Update()
    {
        //確認各方向向量
        Direction();
        //確認腳下物件
        Check_Dark_Plane();
        switch (State)
        {
            case (Player_State.Walking):
                //蹲下
                Squat();
                //跑
                Run();
                //使用瓦爾特
                Use_My_Power();
                //檢測是否可以攀爬
                Climb();
                //走路時跳躍
                New_Jump();
                break;
            case (Player_State.Climbing):
                break;
            case (Player_State.End_Climb):
                break;
            case (Player_State.Start_Climb):
                break;
        }      
        Cursor_Click();
        if (Input.GetButtonDown("Jump"))
        {
            JumpRressed = true;
        }
        if (Is_Invincible)
        {
            Invincible();
        }
    }

    //檢測目前在玩家眼前的物件
    void Eyes()
    {
 
    }
    //移動
    void Movement()
    {
        Rb.velocity = new Vector3(Horizontal_Move * Walk_Speed * Time.fixedDeltaTime, Rb.velocity.y, Rb.velocity.z);
        Anim.SetFloat("Walking", Mathf.Abs(Horizontal_Move) * Walk_Speed);
    }
    //切換動作
    void Switch_Anim()
    {
        if (Anim.GetBool("Jumping"))
        {
            if (Rb.velocity.y < 0)
            {
                Anim.SetBool("Jumping", false);
                Anim.SetBool("Falling", true);
            }
        }
        else if (IsHurt)//受傷狀態
        {
            // Anim.SetFloat("Walking", 0);
            // Anim.SetBool("Hurt", true);
            if (Mathf.Abs(Rb.velocity.x) < 0.1f)//受傷結束
            {
                //Anim.SetBool("Hurt", false);
                //Anim.SetBool("Idle", true);
                IsHurt = false;
            }
        }
        else if (Is_Ground)
        {
            Anim.SetBool("Falling", false);
        }
        if (ladder != null)
        {
            switch (int.Parse(ladder.name))
            {
                case (0):
                    Anim.SetInteger("Ladder_Number", 0);
                    break;
                case (1):
                    Anim.SetInteger("Ladder_Number", 1);
                    break;
                case (2):
                    Anim.SetInteger("Ladder_Number", 2);
                    break;
                case (3):
                    Anim.SetInteger("Ladder_Number", 3);
                    break;
                case (8):
                    Anim.SetInteger("Ladder_Number", 8);
                    break;
                case (9):
                    Anim.SetInteger("Ladder_Number", 9);
                    break;
                case (10):
                    Anim.SetInteger("Ladder_Number", 10);
                    break;
                case (12):
                    Anim.SetInteger("Ladder_Number", 12);
                    break;
                default:
                    Anim.SetInteger("Ladder_Number", -1);
                    break;

            }
        }
        else
        {
            Anim.SetInteger("Ladder_Number", -1);
        }
    }
    //蹲下
    void Squat()
    {
        if (!Keep_Squat)
        {
            if (Input.GetButton("Squat") && State!= Player_State.Climbing)
            {
                Walk_Speed = 8f;
                Anim.SetBool("Squat", true);
                Half_Collider.enabled = false;
            }
            else
            {
                Walk_Speed = 10f;
                Anim.SetBool("Squat", false);
                //Half_Collider.enabled = true;
            }
        }//如果頭頂上沒有障礙物逼我一定要一直維持蹲下
    }
    //跑步
    void Run()
    {
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            Walk_Speed = 15f;
        }
        else if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            Walk_Speed = 10f;
        }
    }
    //跳躍
    void Jump()
    {
        //回復玩家的狀態為一般走路
        State = Player_State.Walking;
        //回復玩家的重力
        Rb.useGravity = true;
        //回復玩家被綁定的軸向
        Rb.constraints = RigidbodyConstraints.FreezeRotation;
        //回復玩家動畫的播放速度
        Anim.speed = 1.0f;
        //取消玩家的攀爬動作
        Anim.SetBool("Climb", false);
        //確認目前不再處於攀爬中
        Is_Climbing = false;
        //確認目前不再接觸地板
        Is_Ground = false;
        //給予玩家向上的施力，以便跳躍
        Rb.velocity = Vector3.up * JumpForce;
        //抓取玩家方向向量，好知道玩家是往哪個方向進跳躍
        Anim.SetFloat("Face_Dircetion", Face_Dircetion);
        //玩家播放跳躍動作
        Anim.SetBool("Jumping", true);
    }
    //跳躍_新(手感更好)
    void New_Jump()
    {
        if (Is_Ground && Input.GetButtonDown("Jump") && State != Player_State.Climbing)
        {
            Is_Ground = false;
            Rb.velocity = new Vector2(Face_Dircetion*Walk_Speed*3f,JumpForce);
            Anim.SetBool("Jumping", true);
            /*if (gameObject.layer == 16)
            {
                Touch_Dark_Point = false;
            }*/
                    
        }
    }
    //玩家狀態回歸原設
    void Get_Normal()
    {
        //取消玩家透明
        Player.color += new Color(0, 0, 0, 0.5f);
        //變更玩家顯示圖層
        Player.sortingOrder = 3;//玩家圖層
        //變更玩家物件圖層
        if (gameObject.layer != 23)
        {
            this.gameObject.layer = 8;//Normal
        }
    }
    //匿蹤
    void Get_Hide()
    {
        // Anim.SetFloat("Face_Dircetion", 0);
        if (State != Player_State.Done)
        {
            Player.color = new Color(255, 255, 255, 0.5f);
            //變更玩家顯示圖層
            Player.sortingOrder = 1;
            //變更玩家物件圖層
            this.gameObject.layer = 10;//Hidding
            if (Is_Hiding)
            {
                //判斷躲藏物件是什麼類型
                switch (Hider.layer)
                {
                    //暗處躲藏點
                    case (11):
                        this.gameObject.layer = 16;//Darking
                        break;
                    //桌子椅子箱子
                    case (12):
                        this.gameObject.layer = 10;//Hidding
                        break;
                }
            }
        }
    }
    //使用瓦爾特
    void Use_My_Power()
    {
        if (Input.GetKeyDown(KeyCode.F) && Take_WaRrTe)
        {
            if (WaRrTe.activeInHierarchy)
            {
                WaRrTe.GetComponent<Click_Play>().Close();
            }
            else
            {
                My_Cursor.Show_WRT();
            }
            WaRrTe.SetActive(!WaRrTe.activeInHierarchy);
        }
    }
    //開發者專用
    public void Developer_Power()
    {
        if (WaRrTe.activeInHierarchy)
        {
            WaRrTe.GetComponent<Click_Play>().Close();
        }
        else
        {
            My_Cursor.Show_WRT();
        }
        WaRrTe.SetActive(!WaRrTe.activeInHierarchy);
    }
    //檢測是否可以攀爬
    private void Climb()
    {
        //可以攀爬，且W或S被按住
        if (CanClimb && Input.GetAxisRaw("Vertical") != 0 && !Is_Climbing && ladder != null && State!=Player_State.Climbing)
        {
            Debug.Log("直接攀爬");
            State = Player_State.Climbing;
            Start_Climb();
        }//爬梯
        else if (CanClimb && TopLadder && !Is_Climbing && Input.GetButtonDown("Squat") && ladder != null && State != Player_State.Climbing)
        {
            Debug.Log("開始攀爬");
            Anim.SetTrigger("Climb_Down");
        }
    }
    //準備爬下
    private void Climb_Down()
    {
        Debug.Log("他要往下囉~");
        //改變角色狀態
        State = Player_State.Start_Climb;
        //回復玩家動畫的播放速度
        Anim.speed = 1.0f;
        //讓角色動起來
        Anim.applyRootMotion = false;
        //確認目前處於攀爬中
        Is_Climbing = true;
    }
    //開始攀爬
    private void Start_Climb()
    {
        if (Input.GetAxis("Vertical") < 0 && Is_Ground)
        {

        }
        else
        {
            Debug.Log("切換狀態，玩家攀爬中");
            State = Player_State.Climbing;
            //讓角色動畫回歸
            Anim.applyRootMotion = true;
            //玩家不再需要檢測是否可以攀爬
            CanClimb = false;
            //紀錄玩家現在已不再接觸地面
            Is_Ground = false;
            //紀錄玩家正在攀爬中
            Is_Climbing = true;
            //檢測梯子位於左方還是右方
            switch (ladder.tag)
            {
                //玩家轉向左邊
                case ("Ladder_Left"):
                    Anim.SetFloat("Ladder_Dircetion", -1);
                    //玩家播放攀爬動畫
                    Anim.SetBool("Climb", true);
                    //綁定玩家X軸，確保玩家在梯子上
                    Rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
                    //將玩家的放置於梯子上
                    transform.position = new Vector2(ladder.transform.position.x + Rope_X, Rb.transform.position.y);
                    //玩家將不再受重力影響，以便上下樓梯
                    Rb.useGravity = false;
                    break;
                //玩家轉向右邊
                case ("Ladder_Right"):
                    Anim.SetFloat("Ladder_Dircetion", 1);
                    //玩家播放攀爬動畫
                    Anim.SetBool("Climb", true);
                    //綁定玩家X軸，確保玩家在梯子上
                    Rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
                    //將玩家的放置於梯子上
                    transform.position = new Vector2(ladder.transform.position.x - Rope_X, Rb.transform.position.y);
                    //玩家將不再受重力影響，以便上下樓梯
                    Rb.useGravity = false;
                    break;
                default:
                    break;
            }
            //玩家將不再受重力影響，以便上下樓梯
            //Rb.useGravity = false;
        }
    }
    //攀爬中
    private void Climbing()
    {
        //檢測到玩家正按著爬行鍵
        if (Input.GetAxis("Vertical") != 0 && ladder != null)
        {
            //玩家持續播放爬行動作
            Anim.speed = 1f;
            //到達頂端只能往下爬
            if (ButttonLadder | TopLadder && Climb_Direction < 0f)
            {
                Rb.velocity = Vector2.up * Climb_Direction * Walk_Speed * Time.fixedDeltaTime;
            }
            //到達頂端且持續向上____壁架攀爬
            else if (TopLadder && Climb_Direction > 0)
            {
                Anim.SetTrigger("Climb_Up");
                return;
            }
            //既不在梯子頂，也不在梯子底，表示還可以繼續爬梯
            else if (!ButttonLadder && !TopLadder)
            {
                Rb.velocity = Vector2.up * Climb_Direction * Walk_Speed * Time.fixedDeltaTime;
            }
        }
        //立即停止不動
        else if (Input.GetButtonDown("Jump"))
        {
            Jump();
            return;
        }
        else
        {
            Rb.velocity = Vector2.zero;
            Anim.speed = 0f;
        }
    }
    //準備結束攀爬
    private void End_Climb()
    {
        //改變角色狀態
        State = Player_State.End_Climb;
        //回復玩家被綁定的軸向
        Rb.constraints = RigidbodyConstraints.FreezeRotation;
        //回復玩家動畫的播放速度
        Anim.speed = 1.0f;
        //取消玩家的攀爬動作
        Anim.SetBool("Climb", false);
        //讓角色動起來
        Anim.applyRootMotion = false;
    }
    //動畫回歸正常
    public void Back_Normal()
    {
        //重製上一次接觸的敵人
        if (Enemy != null)
        {
            Enemy.SetActive(true);
            Enemy = null;
        }
        //重製攀爬動畫判定
        Anim.ResetTrigger("Climb_Up");
        //回歸正常動畫
        Anim.applyRootMotion = true;
        //回歸地面
        Is_Ground = true;
        //判定跳躍建
        JumpRressed = false;
        //確認目前沒有正在跳躍
        Is_Jumping = false;
        //確認目前不是隱藏
        Is_Hiding = false;
        //回復玩家的狀態為一般走路
        State = Player_State.Walking;
        //回復玩家的重力
        Rb.useGravity = true;
        //回復玩家被綁定的軸向
        Rb.constraints = RigidbodyConstraints.FreezeRotation;
        //回復玩家動畫的播放速度
        Anim.speed = 1.0f;
        //取消玩家的攀爬動作
        Anim.SetBool("Climb", false);
        //確認目前不再處於攀爬中
        Is_Climbing = false;
    }
    //抓取各方向向量
    void Direction()
    {
        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            //只要使用左右鍵，就不卡住
            Anim.speed = 1;
            //-1,0,1
           // var Before_Face_Dircetion = Face_Dircetion;
           // Face_Dircetion = Input.GetAxisRaw("Horizontal");
            //確保角色水平方向永不為零(不可能轉正面)
            if (Input.GetAxisRaw("Horizontal") != 0)
            {
                Face_Dircetion = Input.GetAxisRaw("Horizontal");
                Anim.SetFloat("Face_Dircetion", Face_Dircetion);
            }
            /*//確定方向，並使玩家直接轉向
            if (State == Player_State.Walking)
            {
                Anim.SetFloat("Face_Dircetion", Face_Dircetion);
            }*/
           /* //改變視野方向
            switch (Face_Dircetion)
            {
                case (1):
                    Eyes_Line = Mathf.Abs(Eyes_Line);
                    break;
                case (-1):
                    Eyes_Line = -Mathf.Abs(Eyes_Line);
                    break;
            }*/
        }
        //水平移動向量
        Horizontal_Move = Input.GetAxisRaw("Horizontal");//-1~1
        //垂直移動向量
        Climb_Direction = Input.GetAxis("Vertical");
    }
    //爬樓梯
    void Climb_Stairs()
    {
        if (Is_Stairs && Stairs_GameObject != null)
        {
            switch (Stairs_GameObject.name)
            {
                case ("Left_Stairs"):
                    if (Face_Dircetion < 0)
                    {
                        Rb.AddForce(Vector3.up * 4.5f * Rb.mass);
                    }
                    break;
                case ("Right_Stairs"):
                    if (Face_Dircetion > 0)
                    {
                        Rb.AddForce(Vector3.up * 4.5f * Rb.mass);
                    }
                    break;
            }
        }
    }
    //切換鼠標圖片
    private void Change_Cursor()
    {
        My_Cursor.Change_Cursor();
    }
    //回到存檔點
    private void Player_Return()
    {
        //角色進入無敵模式
        Is_Invincible = true;
        //切換角色到無敵圖層
        gameObject.layer = 23;
        //一切回歸原始設定
        Back_Normal();
        if (Save_3 == true)
        {
            transform.position = Save_Pos;
        }
        else
        {
            if (Save_2 == true)
            {
                transform.position = Save_Pos;
            }
            else
            {
                transform.position = Save_Pos;
                if (Save_1 == true)
                {
                    transform.position = Save_Pos;
                }
                else
                {
                    transform.position = Save_Pos;
                }
            }
        }
    }
    //滑鼠點擊_文件
    private void Cursor_Click()
    {
        //射線向量
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //射線抓取物空間
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0) && !WaRrTe.activeInHierarchy)
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Flie")))
            {
                hit.transform.gameObject.GetComponent<Click_File>().Open();
            }
        }
    }
    //隱藏動畫中
    private void State_Get_Hiding()
    {
        State = Player_State.Get_Hide;
    }
    //結束隱藏動畫
    private void State_End_Hiding()
    {
        State = Player_State.Walking;
    }
    //無敵
    private void Invincible()
    {
        //計時無敵時間
        Back_Normal_Time += Time.deltaTime;
        //判斷無敵時間是否到時
        if (Back_Normal_Time >= Invincible_Time)
        {
            //角色回復正常
            Player.material.color = new Color(1f, 1f, 1f, 1f);
            //回復角色圖層
            if (!Is_Hiding)
            {
                gameObject.layer = 8;
            }
            //重製無敵時間
            Back_Normal_Time = 0;
            //結束無敵狀態
            Is_Invincible = false;
        }
        //無敵中_玩家閃爍
        else
        {
            if (Flag > 0)
            {
                if (Player.material.color.a >= 1)
                {
                    Flag = -Mathf.Abs(Flag);
                }
                else
                {
                    Player.material.color += new Color(0, 0, 0, 0.5f) * Invincible_Speed * Time.deltaTime;
                }
            }
            else if (Flag < 0)
            {
                if (Player.material.color.a <= 0)
                {
                    Flag = Mathf.Abs(Flag);
                }
                else
                {
                    Player.material.color -= new Color(0, 0, 0, 0.5f) * Invincible_Speed * Time.deltaTime;
                }
            }

        }
    }
    //檢視暗處躲藏點
    private void Check_Dark_Plane()
    {
        if (State != Player_State.Done)
        {
            if (Hider != null && !Is_Hiding)
            {
                Hider = null;
               // Touch_Dark_Point = false;
            }
            else if (Hider == null)
            {
                RaycastHit hit;
                Ray ray = new Ray(transform.position, new Vector3(0, -0.1f, 0));
                Debug.DrawRay(transform.position, new Vector3(0, -0.1f, 0), Color.red);
                if (Physics.Raycast(ray, out hit, 0.1f, LayerMask.GetMask("Dark_Plane")))
                {
                    if (hit.transform.gameObject.tag == "Dark_Point")
                    {
                       // Touch_Dark_Point = true;
                       //Hider = hit.transform.gameObject;
                        if (Input.GetKeyDown(KeyCode.E) && !Is_Hiding)
                        {
                            Hider = hit.transform.gameObject;
                            Is_Hiding = true;
                            Can_Hide = false;
                            Anim.SetTrigger("Hide_Plane");
                            Anim.speed = 1;
                        }
                        else if (!Is_Hiding)
                        {
                            Can_Hide = true;
                        }
                    }
                }
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        //給予向上的力，讓角色可以爬樓梯
        if (collision.gameObject.tag == "Down" && Stairs_GameObject == null)
        {
            Stairs_GameObject = collision.gameObject;
            Is_Stairs = true;
        }
        //接觸暗處躲藏點(需爬下)
        if (collision.gameObject.tag == "Dark_Point" )
        {
            Touch_Dark_Point = true;
            /*if (Input.GetKeyDown(KeyCode.E) && !Is_Hiding)
            {
                Hider = collision.gameObject;
                Is_Hiding = true;
                Can_Hide = false;
                Anim.SetTrigger("Hide_Plane");
            }*/
            /*else if (!Is_Hiding)
            {
                Can_Hide = true;
            }*/
        }
        //接觸暗處躲藏點(直接穿透)
        if (collision.gameObject.tag == "Dark_Hide" && Hider == null)
        {
            if (Input.GetKeyDown(KeyCode.E) && !Is_Hiding && State != Player_State.Done)
            {
                Hider = collision.gameObject;
                Is_Hiding = true;
                Can_Hide = false;
                Get_Hide();
            }
            /*else if (!Is_Hiding)
            {
                Can_Hide = true;
            }*/
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        //踩在地面或樓梯上或移動平台上
        if (collision.gameObject.tag == "Plane" || collision.gameObject.tag == "Down" || collision.gameObject.tag == "Move_Plane" || collision.gameObject.tag == "Dark_Point")
        {
            Is_Ground = true;
            JumpRressed = false;
            Is_Jumping = false;
        }
        //接觸到使玩家受傷物件
        switch (collision.gameObject.tag)
        {
            case ("Soldier"):
                Enemy = collision.transform.gameObject;
                Enemy.SetActive(false);
                State = Player_State.Done;
                Anim.speed = 1;
                Anim.SetTrigger("Robot_Done");
                break;
            case ("Gem"):
                Enemy = collision.transform.gameObject;
                Enemy.SetActive(false);
                State = Player_State.Done;
                Anim.speed = 1;
                Anim.SetTrigger("C_Done");
                break;
            case ("crystal"):
                State = Player_State.Done;
                Anim.speed = 1;
                Anim.SetTrigger("Crystal_Done");
                break;
        }
        //被消失的受傷害判斷_玩家會被擊退
        /*if (collision.gameObject.tag == "Soldier")
        {
            if (transform.position.x > collision.transform.position.x)
            {
                Rb.velocity = new Vector3(1f, Rb.velocity.y, Rb.velocity.z);
                IsHurt = true;
            }
            else if (transform.position.x < collision.transform.position.x)
            {
                Rb.velocity = new Vector3(-1f, Rb.velocity.y, Rb.velocity.z);
                IsHurt = true;
            }
        }*/
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Down")
        {
            Is_Stairs = false;
            Stairs_GameObject = null;
        }
        if (collision.gameObject.tag == "Dark_Point")
        {
            Touch_Dark_Point = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        //碰觸存檔點
        if (other.gameObject.tag == "Save")
        {
            switch (other.name)
            {
                case ("存檔點_1"):
                    Save_1 = true;
                    Save_Pos = other.transform.position;
                    break;
                case ("存檔點_2"):
                    Save_2 = true;
                    Save_Pos = other.transform.position;
                    break;
                case ("存檔點_3"):
                    Save_3 = true;
                    Save_Pos = other.transform.position;
                    break;
            }
        }
        if (other.tag == "Wa_R_T" && Touch_WRT != null)
        {
            Touch_WRT.SetActive(true);
        }
        switch (other.gameObject.tag)
        {
            case ("Overview_1"):
                OverView[0].SetActive(true);
                Destroy(other.gameObject);
                break;
            case ("Overview_2"):
                OverView[1].SetActive(true);
                Destroy(other.gameObject);
                break;
            case ("Overview_3"):
                OverView[2].SetActive(true);
                Destroy(other.gameObject);
                break;
            default:
                break;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        //檢測頭頂上是否有平面
        if (other.tag == "Plane")
        {
            Keep_Squat = true;
        }
        //接觸到瓦爾特
        if (other.tag == "Wa_R_T")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                other.gameObject.GetComponent<SpriteRenderer>().sprite = Taking;
                if (Touch_WRT != null)
                {
                    Touch_WRT.SetActive(false);
                    Touch_WRT = null;
                    Take_WRF.Open_Image();
                }
                Take_WaRrTe = true;
            }
        }
        //接觸躲藏點
        if (other.tag == "Hider" && State != Player_State.Done)
        {
            if (Input.GetKeyDown(KeyCode.E) && !Is_Hiding && !Touch_Dark_Point)
            {
                
                Hider = other.gameObject;
                Is_Hiding = true;
                Can_Hide = false;
                Anim.SetTrigger("Hide");
                Anim.speed = 1;
               // Anim.SetFloat("Face_Dircetion", 0f);
            }
            else if (!Is_Hiding)
            {
                Can_Hide = true;
            }
        }     
    }
    private void OnTriggerExit(Collider other)
    {
        //離開地面
        if (other.tag == "Plane")
        {
            Keep_Squat = false;
        }
        /* if (other.tag == "Ladder_Left" || other.tag == "Ladder_Right")
         {
             Debug.Log("離開繩子");
             //回復玩家的狀態為一般走路
             State = Player_State.Walking;
             //回復玩家的重力
             Rb.useGravity = true;
             //回復玩家被綁定的軸向
             Rb.constraints = RigidbodyConstraints.FreezeRotation;
             //回復玩家動畫的播放速度
             Anim.speed = 1.0f;
             //取消玩家的攀爬動作
             Anim.SetBool("Climb", false);
             //確認目前不再處於攀爬中
             Is_Climbing = false;
         }*/
        //離開瓦爾特
        /*if (other.tag == "Wa_R_T" && Take_WaRrTe)
        {
            Take_WaRrTe = false;
        }*/
        //離開躲藏點
        if (other.tag == "Hider" && gameObject.layer == 10 | gameObject.layer == 8)
        {
            Get_Normal();
            Hider = null;
            Is_Hiding = false;
            Can_Hide = false;
        }
        /* if (other.tag == "Dark_Point" && gameObject.layer == 16)
         {
             Get_Normal();
             Hider = null;
             Is_Hiding = false;
             Can_Hide = false;
         }*/
        if (other.tag == "Dark_Hide" && gameObject.layer == 16)
        {
            Get_Normal();
            Hider = null;
            Is_Hiding = false;
            Can_Hide = false;
        }
        if (other.tag == "Check_Dark" && gameObject.layer == 16)
        {
            Get_Normal();
            Hider = null;
            Is_Hiding = false;
            Can_Hide = false;
        }
        if (other.tag == "Wa_R_T" && Touch_WRT != null)
        {
            Touch_WRT.SetActive(false);
        }
    }
}
