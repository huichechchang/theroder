﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Main_Menu : MonoBehaviour
{
    public AudioMixer audioMixer;


    //暫停遊戲
    public void Stop_Game()
    {
        Time.timeScale = 0;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }
    //繼續遊戲
    public void Keep_Game()
    {
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }
    //設定混音器數值
    public void Mix_Audio_Manager(float value)
    {
        audioMixer.SetFloat("???", value);
    }
}
