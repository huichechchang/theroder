﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Click_Play : MonoBehaviour
{
    [Header("玩家物件")]
    public Player_Controller Player;

    [Header("瓦爾特功能轉輪")]
    public GameObject Roll = null;
    [Header("確認是否決定改變大小的按鈕")]
    public GameObject CheckButton = null;
    [Header("改變大小的拉霸")]
    public GameObject Bar = null;
    [Header("被操作使用的敵人元件")]
    public Enemy_Soldier Enemy_Gameobject = null;
    [Header("被操控使用的心臟物件")]
    [SerializeField]
    private Heart Heart = null;

    [Header("射線長度")]
    public float Ray_Line = 0f;
    [Header("調整拉霸的位置")]
    [SerializeField]
    private float X_Axis = 0, Y_Axis = 0;
    [Header("調整敵人大小的尺寸")]
    [SerializeField]
    private float Max = 2.0f, Min = 0.5f;
    [Header("瓦爾特推動敵人的力")]
    [SerializeField]
    private float PushForce = 0;
    private enum WaTrTe { Big_Smell, Push };
    [Header("瓦爾特狀態")]
    [SerializeField]
    private WaTrTe WaTrTeState;

    [Header("抓取滑鼠物件")]
    [SerializeField]
    private Mouse_Cursor My_Cursor = null;

    void Start()
    {
        Roll.SetActive(true);
        Player = transform.parent.GetComponent<Player_Controller>();
    }
    void Update()
    {
        //使用射線
        Using();
        //如果瓦爾特有探測到敵人便使用目前狀態功能
        State();
        //轉動並改變瓦爾特功能
        Get_Roll();
        //改變瓦爾特的方向
        Chang_Dircetion();
    }
    //使用瓦爾特
    void Using()
    {
        //射線向量
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //射線抓取物空間
        RaycastHit hit;
        //滑鼠點擊左鍵，固定敵人
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Enemy","UI")))
            {
                Debug.Log(hit.collider.gameObject.name);
                if (hit.transform.gameObject.GetComponent<Enemy_Soldier>() && hit.transform.gameObject.tag =="Gem")
                {
                    //鼠標確認鎖定
                    My_Cursor.Check_Lock();
                    //確認鎖定_出現特效
                    hit.transform.GetChild(2).gameObject.SetActive(true);
                    //紀錄被抓取的敵人物件
                    Enemy_Gameobject = hit.transform.gameObject.GetComponent<Enemy_Soldier>();
                    //敵人轉換為_被抓住中
                    Enemy_Gameobject.Be_Catching();
                   /* //將狀態機定為(抓取)
                    Enemy_Gameobject.state = Enemy_Manager.State.BeCatch;
                    //標記這個敵人被抓住了
                    Enemy_Gameobject.Be_Catch = true;*/
                    //將拉霸初始設定為此敵人大小比例
                    var Enemy_Size = (Mathf.Abs(Enemy_Gameobject.transform.localScale.x) / 0.01f - Min) / (Max - Min);
                    Bar.GetComponent<Scrollbar>().value = Enemy_Size;
                }
                else if (hit.transform.gameObject.GetComponent<Heart>())
                {
                    //鼠標確認鎖定
                    My_Cursor.Check_Lock();
                    //將拉霸初始設定為基礎大小比例比例
                    Bar.GetComponent<Scrollbar>().value = 0.1f;
                    //紀錄心臟物件
                    Heart = hit.transform.gameObject.GetComponent<Heart>();
                }
            }
        }
        //滑鼠點擊右鍵，放開敵人
        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("典籍又見");
            if (Enemy_Gameobject != null)
            {
                Check_Enemy_Size();
                /*//鼠標取消鎖定
                My_Cursor.Out_Lock();
                //確認取消_關閉特效
                Enemy_Gameobject.transform.GetChild(2).gameObject.SetActive(false);
                Enemy_Gameobject.Retrun_Normal_Size();
                Enemy_Gameobject = null;
                //Check_Enemy_Size();*/
            }
        }
    }
    //狀態機
    void State()
    {
        switch (WaTrTeState)
        {
            case (WaTrTe.Big_Smell):
                BigSemll_State();
                break;
            case (WaTrTe.Push):
                Push_State();
                break;
        }
    }
    //改變瓦爾特功能狀態以及轉動狀態轉輪
    void Get_Roll()
    {
        Roll.SetActive(true);
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            Roll.GetComponent<RectTransform>().localEulerAngles += new Vector3(0, 0, 180f);
            switch (WaTrTeState)
            {
                case (WaTrTe.Big_Smell):
                    WaTrTeState = WaTrTe.Push;
                    break;
                case (WaTrTe.Push):
                    WaTrTeState = WaTrTe.Big_Smell;
                    break;
            }//改變瓦爾特功能狀態
        }
    }
    //改變大小的狀態
    void BigSemll_State()
    {
        //如果有偵測到敵人，使用功能
        if (Enemy_Gameobject != null)
        {
            //開啟屬於此狀態的物件
            Bar.SetActive(true);

            //調整拉霸的位置
            Bar.GetComponent<RectTransform>().transform.position =
                (RectTransformUtility.WorldToScreenPoint(Camera.main, Enemy_Gameobject.transform.position) + //瓦爾特的世界座標轉為UI座標
                (new Vector2(X_Axis, Y_Axis)));//調整拉霸該待在的位置
            //將狀態機定為(抓取)
           /* Enemy_Gameobject.state = Enemy_Manager.State.BeCatch;
            //標記這個敵人被抓住了
            Enemy_Gameobject.Be_Catch = true;*/
                //抓取拉霸的數值，好確定要讓敵人變多大多小
                var Size = Bar.GetComponent<Scrollbar>().value;
            //制定敵人大小
            Enemy_Gameobject.Big_Or_Smell(Min, Max, Size);
        }
        //如果有偵測到心臟碎片，使用功能
        if (Heart != null)
        {
            if (Heart.Is_Max)
            {
                Bar.SetActive(false);
            }
            else
            {
                Bar.SetActive(true);
                //調整拉霸的位置
                Bar.GetComponent<RectTransform>().transform.position =
                    (RectTransformUtility.WorldToScreenPoint(Camera.main, Heart.transform.position) + //瓦爾特的世界座標轉為UI座標
                    (new Vector2(X_Axis, Y_Axis)));//調整拉霸該待在的位置
                                                   //抓取拉霸的數值，好確定要讓心臟碎片變多大多小
                var Size = Bar.GetComponent<Scrollbar>().value;
                //制定心臟碎片大小
                Heart.Big_Or_Smell(Min, Max, Size);
            }
        }
    }
    //推動敵人的狀態
    void Push_State()
    {
        Bar.gameObject.SetActive(false);
        //CheckButton.SetActive(false);

        if (Enemy_Gameobject != null)
        {
            //將狀態機定為(抓取)
           /* Enemy_Gameobject.state = Enemy_Manager.State.BeCatch;
            //標記這個敵人被抓住了
            Enemy_Gameobject.Be_Catch = true;*/
            /* //清空敵人物件，確保他不會一直被施力
             Enemy_Gameobject = null;
             //進入冷卻
             Player.Is_Cold = true;*/
        }
    }
    //改變瓦爾特方向
    void Chang_Dircetion()
    {
        if (Player.Face_Dircetion > 0)
        {
            //改變射線方向
            Ray_Line = Mathf.Abs(Ray_Line);

            //改變施力方向
            PushForce = Mathf.Abs(PushForce);

            //改變UI位置
            transform.localPosition = new Vector3(Mathf.Abs(transform.localPosition.x), 0, 0);
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x),
                                                          transform.localScale.y,
                                                          transform.localScale.z);
        }
        else if (Player.Face_Dircetion < 0)
        {
            //改變射線方向
            Ray_Line = -Mathf.Abs(Ray_Line);

            //改變施力方向
            PushForce = -Mathf.Abs(PushForce);

            //改變UI位置
            transform.localPosition = new Vector3(-Mathf.Abs(transform.localPosition.x), 0, 0);
            transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x),
                                                         transform.localScale.y,
                                                         transform.localScale.z);
        }
    }
    //關閉瓦爾特(物件清空)
    public void Close()
    {
        My_Cursor.Close_WRT();
        Roll.SetActive(false);
        Bar.SetActive(false);
        if (Enemy_Gameobject != null)
        {
            Enemy_Gameobject.transform.GetChild(2).gameObject.SetActive(false);
            //解除敵人被抓住狀態
            Enemy_Gameobject.End_Catch();
        }
        Enemy_Gameobject = null;
    }
    //按下確認鍵，確定敵人大小
    public void Check_Enemy_Size()
    {
       // My_Cursor.Close_WRT();
        //確認敵人大小
        if (Enemy_Gameobject != null && Enemy_Gameobject.Be_Catch && WaTrTeState == WaTrTe.Big_Smell)
        {
            //鼠標取消鎖定
            My_Cursor.Out_Lock();
            //關閉鎖定敵人的特效
            Enemy_Gameobject.transform.GetChild(2).gameObject.SetActive(false);
            //紀錄敵人現在尺寸
            Enemy_Gameobject.Normal_Size = Enemy_Gameobject.transform.localScale;
            //進入冷卻
            Player.Is_Cold = true;
            //解除敵人被抓住狀態
            Enemy_Gameobject.End_Catch();
            //Enemy_Gameobject.Be_Catch = false;
            //讓敵人進入警戒
            //Enemy_Gameobject.Worring();
            //清空敵人物件，確保他不再被一直偵測
            Enemy_Gameobject = null;
            //關閉底下的拉霸
            Bar.SetActive(false);
        }
        //確定心臟大小
        if (Heart != null  && WaTrTeState == WaTrTe.Big_Smell)
        {
            //鼠標取消鎖定
            My_Cursor.Out_Lock();
            //紀錄敵人現在尺寸
            //Enemy_Gameobject.Normal_Size = Enemy_Gameobject.transform.localScale;
            //進入冷卻
            //Player.Is_Cold = true;
            //解除敵人被抓住狀態
            //Enemy_Gameobject.Be_Catch = false;
            //讓敵人進入警戒
            //Enemy_Gameobject.Worring();
            //清空敵人物件，確保他不再被一直偵測
            // Enemy_Gameobject = null;
            //關閉底下的拉霸
            Bar.SetActive(false);
        }
        //確定推動敵人
        else if (Enemy_Gameobject != null && Enemy_Gameobject.Be_Catch && WaTrTeState == WaTrTe.Push)
        {
            Debug.Log("敵人被推走了");
            //鼠標取消鎖定
            My_Cursor.Out_Lock();
            //關閉鎖定敵人的特效
            Enemy_Gameobject.transform.GetChild(2).gameObject.SetActive(false);
            //抓取敵人物件中被推動的含式，並施予一個量值
            Enemy_Gameobject.Push(PushForce);
            //進入冷卻
            Player.Is_Cold = true;
            //解除敵人被抓住狀態
            Enemy_Gameobject.End_Catch();
            //Enemy_Gameobject.Be_Catch = false;
            //讓敵人進入警戒
           // Enemy_Gameobject.Worring();
            //清空敵人物件，確保他不再被一直偵測
            Enemy_Gameobject = null;
            //關閉底下的拉霸
            Bar.SetActive(false);
        }
    }

}
