﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.Video;

public class Scence : MonoBehaviour
{
    [Header("要載入的場景編號")]
    [SerializeField]
    private int Scence_ID = 1;
    [Header("要撥放動畫的物件")]
    [SerializeField]
    private GameObject Video_Object = null;
    [Header("要撥放動畫的元件")]
    [SerializeField]
    private VideoPlayer Video = null;

    AsyncOperation async;
    void Start()
    {
        if (Video_Object != null)
        {
            Video = Video_Object.GetComponent<VideoPlayer>();
        }
    }
    public void Change_Scence()
    {
        Video_Object.SetActive(true);
        if (Video == null)
        {
            Debug.Log("直接仔入場警");
            SceneManager.LoadSceneAsync(Scence_ID);
        }
        else
        {
            Debug.Log("後臺仔入場警");
            Video.Play();
            async = SceneManager.LoadSceneAsync(Scence_ID);
            async.allowSceneActivation = false;
            Video.loopPointReached += End_Video;
        }
    }
    public void Skip_Video()
    {
        async.allowSceneActivation = true;
    }
    void End_Video(UnityEngine.Video.VideoPlayer video)
    {
        if (!async.allowSceneActivation)
        {
            async.allowSceneActivation = true;
        }
    }
}
