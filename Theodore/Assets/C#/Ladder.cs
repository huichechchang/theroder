﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    private enum LadderPart { Top, Button, Complete,Done};
    [SerializeField] LadderPart Part = LadderPart.Complete;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player_Controller>())
        {
            Player_Controller Player = other.GetComponent<Player_Controller>();
            switch (Part)
            {
                case (LadderPart.Top):
                    Player.TopLadder = true;
                    Player.ladder = this.gameObject;
                    break;
                case (LadderPart.Button):
                    Player.ButttonLadder = true;
                    break;
               /* case (LadderPart.Complete):
                    if (Player.ladder == null)
                    {
                        Player.ladder = this;
                    }
                    Player.CanClimb = true;
                    break;*/
                default:
                    break;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Player_Controller>())
        {
            Player_Controller Player = other.GetComponent<Player_Controller>();
            switch (Part)
            {
                case (LadderPart.Complete):
                    if (Player.ladder == null)
                    {
                        Player.ladder = this.gameObject;
                    }
                    Player.CanClimb = true;
                    break;
                default:
                    break;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Player_Controller>())
        {
            //抓取玩家元件
            Player_Controller Player = other.GetComponent<Player_Controller>();
            switch (Part)
            {
                case (LadderPart.Top):
                    Player.TopLadder = false;
                    Player.ladder = null;
                    //Player.Climb_Animation = false;
                    break;
                case (LadderPart.Button):
                    Player.ButttonLadder = false;
                    Player.ladder = null;
                    break;
                case (LadderPart.Complete):
                    if (Player.State == Player_Controller.Player_State.Climbing)
                    {
                        Player.CanClimb = false;
                        Player.ladder = null;
                        //回復玩家的狀態為一般走路
                       // Player.State = Player_Controller.Player_State.Walking;
                        //回復玩家的重力
                        Player.Rb.useGravity = true;
                        //回復玩家被綁定的軸向
                        Player.Rb.constraints = RigidbodyConstraints.FreezeRotation;
                        //回復玩家動畫的播放速度
                        Player.Anim.speed = 1.0f;
                        //取消玩家的攀爬動作
                        Player.Anim.SetBool("Climb", false);
                        //確認目前不再處於攀爬中
                        Player.Is_Climbing = false;
                    }
                    else
                    {
                        Player.CanClimb = false;
                        Player.ladder = null;
                    }
                    break;
                case (LadderPart.Done):
                    //接觸底端且玩家是向下的
                    if (Player.State == Player_Controller.Player_State.Climbing && Input.GetAxis("Vertical")<0)
                    {
                        Debug.Log("Leave_Ladder");
                        Player.CanClimb = false;
                        Player.ladder = null;
                        //回復玩家的狀態為一般走路
                        // Player.State = Player_Controller.Player_State.Walking;
                        //回復玩家的重力
                        Player.Rb.useGravity = true;
                        //回復玩家被綁定的軸向
                        Player.Rb.constraints = RigidbodyConstraints.FreezeRotation;
                        //回復玩家動畫的播放速度
                        Player.Anim.speed = 1.0f;
                        //取消玩家的攀爬動作
                        Player.Anim.SetBool("Climb", false);
                        //確認目前不再處於攀爬中
                        Player.Is_Climbing = false;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
