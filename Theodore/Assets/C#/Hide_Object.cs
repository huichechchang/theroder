﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide_Object : MonoBehaviour
{
    [Header("描邊材質")]
    [SerializeField]
    private Material OutLine_Mat = null;

    [Header("原始材質")]
    [SerializeField]
    private Material Orignal_Mat = null;
    private void Awake()
    {
        Orignal_Mat = GetComponent<Renderer>().material;
        //OutLine_Mat.mainTexture = Orignal_Mat.mainTexture;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            GetComponent<Renderer>().material = OutLine_Mat;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            GetComponent<Renderer>().material = Orignal_Mat;
        }
    }
}
