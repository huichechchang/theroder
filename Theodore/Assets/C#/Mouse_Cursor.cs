﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mouse_Cursor : MonoBehaviour
{
    [Header("自訂滑鼠大小")]
    [SerializeField]
    private float Cursor_Size = 0f;

    [Header("自訂瓦爾特滑鼠大小")]
    [SerializeField]
    private float WRT_Cursor_Size = 0f;

    [Header("瓦爾特模式鼠標")]
    [SerializeField]
    private Sprite WRT_Cursor = null;

    [Header("抓取動畫機")]
    [SerializeField]
    public Animator Cursor_am = null;

    [Header("判定是關閉還是開啟")]
    [SerializeField]
    private bool Is_Using = false;

    [Header("判斷是否可以鎖定")]
    [SerializeField]
    private bool Can_Lock = false;

    [Header("判斷是否已經鎖定")]
    [SerializeField]
    private bool Locking = false;

    [Header("調整滑鼠位置(一般模式)")]
    [SerializeField]
    private Vector3 Mouse_Pos = new Vector3(0, 0, 0);

    [Header("調整滑鼠位置(瓦爾特模式)")]
    [SerializeField]
    private Vector3 WRT_Mouse_Pos = new Vector3(0, 0, 0);

    private void Awake()
    {
        Cursor_am = GetComponent<Animator>();
    }
    private void Start()
    {
        Cursor.visible = false;
        GetComponent<RectTransform>().localScale = new Vector3(Cursor_Size, Cursor_Size, Cursor_Size);
    }
    void Update()
    {
        // GetComponent<RectTransform>().localScale = new Vector3(Cursor_Size, Cursor_Size, Cursor_Size);
        if (!Is_Using)
        {
            transform.position = Input.mousePosition + Mouse_Pos;
        }
        else
        {
            transform.position = Input.mousePosition + WRT_Mouse_Pos;
        }
        /*var RectTransform = GetComponent<RectTransform>();
        RectTransform.anchoredPosition = Input.mousePosition;*/
    }

    //改變鼠標模樣
    public void Return_Size()
    {
        GetComponent<RectTransform>().localScale = new Vector3(Cursor_Size, Cursor_Size, Cursor_Size);
    }

    public void WRT_Size()
    {
        GetComponent<RectTransform>().localScale = new Vector3(WRT_Cursor_Size, WRT_Cursor_Size, WRT_Cursor_Size);
    }

    public void Show_WRT()
    {
        WRT_Size();
        Is_Using = true;
        Cursor_am.SetBool("Using_WRT", true);
    }
    public void Change_Cursor()
    {
        if (Is_Using)
        {
            Is_Using = false;
            Cursor_am.SetBool("Using_WRT", false);
        }
        else
        {
            Is_Using = true;
            WRT_Size();
            Cursor_am.SetBool("Using_WRT", true);
        }
    }

    //接近可鎖定區域
    public void Start_Lock()
    {
        if (Cursor_am.GetBool("Using_WRT"))
        {
            Cursor_am.SetBool("Can_Lock", true);
        }
    }
    //離開可鎖定區域
    public void End_Lock()
    {
        if (Cursor_am.GetBool("Using_WRT"))
        {
            Cursor_am.SetBool("Can_Lock", false);
        }
    }
    //點擊確認鎖定
    public void Check_Lock()
    {
        if (Cursor_am.GetBool("Can_Lock"))
        {
            Return_Size();
            Cursor_am.SetBool("Check_Lock", true);
        }
    }
    //右鍵取消鎖定
    public void Out_Lock()
    {
        if (Cursor_am.GetBool("Check_Lock"))
        {
            WRT_Size();
            Cursor_am.SetBool("Check_Lock", false);
        }
    }

    public void Close_WRT()
    {
        Is_Using = false;
        Cursor_am.SetBool("Using_WRT", false);
        Cursor_am.SetBool("Can_Lock", false);
        Cursor_am.SetBool("Check_Lock", false);
    }
}
