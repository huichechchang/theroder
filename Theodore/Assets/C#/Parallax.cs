﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    /// <summary>
    /// 視覺差
    /// </summary>
    public Transform Cam;
    public float MoveRate;
    private float Start_PosX,Start_PosY;
    public bool Lock_Y = false;
    void Start()
    {
        Start_PosX = transform.position.x;
        Start_PosY = transform.position.y;
    }
    void Update()
    {
        if (Lock_Y)
        {
            transform.position = new Vector2(Start_PosX + Cam.position.x * MoveRate, transform.position.y);
        }
        else
        {
            transform.position = new Vector2(Start_PosX + Cam.position.x * MoveRate, Start_PosY+Cam.position.y*MoveRate);
        }
    }
}
