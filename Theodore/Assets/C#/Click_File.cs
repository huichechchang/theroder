﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click_File : MonoBehaviour
{
    [Header("要打開的文件")]
    [SerializeField]
    private GameObject File = null;
    [Header("2D_OutLine材質")]
    [SerializeField]
    private Material OutLine_Mat = null;
    [Header("原始材質")]
    [SerializeField]
    private Material Original_Mat = null;
    private void Awake()
    {
        Original_Mat = transform.GetChild(0).GetComponent<SpriteRenderer>().material;
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && File.activeInHierarchy)
        {
            Close();
        }
    }
    public void Open()
    {
        File.SetActive(true);
    }

    public void Close()
    {
        File.SetActive(false);
    }
    private void OnMouseEnter()
    {
        transform.GetChild(0).GetComponent<SpriteRenderer>().material = OutLine_Mat;
    }
    private void OnMouseExit()
    {
        transform.GetChild(0).GetComponent<SpriteRenderer>().material = Original_Mat;
    }
}
