﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime;
using System.IO;
using UnityEngine;

public class Save_Load : MonoBehaviour
{
    public GameObject player;
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Save();
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            Load();
        }
    }
    public void Save()
    {
        Data data = new Data();
        data.Player_x = player.transform.position.x;
        data.Player_y = player.transform.position.y;
        data.Player_z = player.transform.position.z;
        //JsonUtility.ToJson(data); //轉換Json字串 或許以後會用到
        FileStream Fs = new FileStream(Application.dataPath+ "/Save.txt",FileMode.Create);
        StreamWriter Sw = new StreamWriter(Fs);
        Sw.WriteLine(data.Player_x);
        Sw.WriteLine(data.Player_y);
        Sw.WriteLine(data.Player_z);
        Sw.Close();
        Fs.Close();
        Debug.Log("儲存成功");
        
    }
    public void Load()
    {
        Data data = new Data();//重新存檔   
        FileStream Fs = new FileStream(Application.dataPath + "/Save.txt", FileMode.Open);
        StreamReader Sr = new StreamReader(Fs);
        data.Player_x = float.Parse(Sr.ReadLine());
        data.Player_y = float.Parse(Sr.ReadLine());
        data.Player_z = float.Parse(Sr.ReadLine());
        Debug.Log("讀取成功");
        player.transform.position = new Vector3(data.Player_x, data.Player_y, data.Player_z);
    }
}

[System.Serializable]
public class Data
{
    public float Player_x;//玩家X軸
    public float Player_y;//玩家Y軸
    public float Player_z;//玩家Z軸

    public int Heart_int;//獲得心臟碎片數量
    public int Flie_int;//獲得文件碎片數量
    public int Area;//擴展區域
    public int Enemy;        
}
