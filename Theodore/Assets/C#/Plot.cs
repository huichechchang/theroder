﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Plot : MonoBehaviour
{
    [Header("要顯示的對話圖片")]
    [SerializeField]
    private Sprite[] sprites = null;
    [Header("要顯示對話的物件")]
    [SerializeField]
    private Image image = null;
    [Header("接著要放的過場動畫")]
    [SerializeField]
    private GameObject OverView = null;
    [Header("心臟碎片謎團未解開")]
    [SerializeField]
    private Heart Heart_Max = null;

    private void Awake()
    {
        image.sprite = sprites[0];
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            image.gameObject.SetActive(true);
            Time.timeScale = 0;
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }
    public void Next_Sprite()
    {
        for (int i = 0; i <= sprites.Length - 1; i++)
        {
            if (image.sprite == sprites[i])
            {
                if (i == sprites.Length - 1)
                {
                    if (OverView != null)
                    {
                        image.gameObject.SetActive(false);
                        OverView.SetActive(true);
                        Time.timeScale = 1;
                    }
                    else
                    {
                        image.gameObject.SetActive(false);
                        Time.timeScale = 1;
                    }
                    break;
                }
                else
                {
                    image.sprite = sprites[i + 1];
                    break;
                }
            }
        }
    }
    public void Open_Image()
    {
        image.sprite = sprites[0];
        image.gameObject.SetActive(true);
        Time.timeScale = 0;
        gameObject.GetComponent<BoxCollider>().enabled = false;
    }
    public void Close_Image()
    {
        gameObject.GetComponent<BoxCollider>().enabled = false;
        Time.timeScale = 1;
    }
}
