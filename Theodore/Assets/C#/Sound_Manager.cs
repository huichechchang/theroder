﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound_Manager : MonoBehaviour
{
    public static Sound_Manager Instance_sound_Manager;
    public AudioSource audioSource;

    [SerializeField]private AudioClip Jump_Source, Walk_Source;

    private void Awake()
    {
        Instance_sound_Manager = this;
    }
    public void Jump_Audio()
    {
        audioSource.clip = Jump_Source;
        audioSource.Play();
    }
}
