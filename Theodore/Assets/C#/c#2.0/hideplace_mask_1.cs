﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hideplace_mask_1 : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject hideplace;//區域遮擋

    void OnTriggerEnter(Collider other)//玩家進入遮擋的collider
    {
        if(other.tag=="Player")
        {
            Destroy(hideplace.gameObject); ;//遮擋刪除,玩家看見區域全貌
        }
    }
}
