﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class area_mask : MonoBehaviour
{

    public GameObject area_m;//區域遮擋1
    public GameObject ganchi;
    private float Up_float = 0.006f;
    public bool Open = false;
    private Vector3 Area_pos;
    // Start is called before the first frame update
    void Start()
    {
        Area_pos = area_m.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(Open==true)
        {
            Open_mask();
        }
    }

    public void Open_mask()
    {
        if (Area_pos.y > 5.25f)
        {
            Destroy(transform.GetComponent<area_mask>());
        }
        area_m.transform.position = new Vector3(Area_pos.x,Area_pos.y+=Up_float,Area_pos.z);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && Input.GetKeyDown(KeyCode.E))
        {
            Open = true;
        }
    }
}
