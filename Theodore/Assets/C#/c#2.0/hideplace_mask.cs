﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hideplace_mask : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject hideplace;//密室遮擋1

    void OnTriggerEnter(Collider other)//玩家進入遮擋1的collider
    {
        if(other.tag=="Player")
        {
           hideplace.SetActive(false);//遮擋消失,玩家看見密室全貌
           //Debug.Log("false");
        }
    }
    void OnTriggerExit(Collider other)//玩家離開遮擋1的collider
    {
        if(other.tag=="Player")
        {
            hideplace.SetActive(true);//遮擋出現,玩家無法看見密室
            //Debug.Log("true");
        }
    }
}
