﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    [Header("檢測是否達到最大值")]
    [SerializeField]
    public bool Is_Max = false;
    [Header("抓取因培養罐破裂而移動的牆")]
    [SerializeField]
    private GameObject Wall;
    [Header("控制石牆要往哪移動")]
    [SerializeField]
    private Vector3 Move_Dircetion = Vector3.zero;
    [Header("控制石牆移動時間")]
    [SerializeField]
    private float Move_Time = 0f;
    [Header("移動計時")]
    [SerializeField]
    private float Cut_Time = 0f;

    [Header("筆記本的心臟碎片")]
    [SerializeField]
    private GameObject Book_Heart = null;

    [Header("解謎前動畫_1")]
    [SerializeField]
    private Plot plot_1 = null;
    [Header("解謎前動畫_2")]
    [SerializeField]
    private Plot plot_2 = null;
    [Header("解謎後動畫_1")]
    [SerializeField]
    private Plot plot = null;
    void Start()
    {

    }
    void Update()
    {
        if (Is_Max)
        {
            Open_Wall();
        }
    }
    public void Big_Or_Smell(float Min, float Max, float Size)
    {
        var Heart_Size = Mathf.Lerp(Min, Max, Size);
        transform.localScale = new Vector2(Mathf.Abs(0.015f * Heart_Size), 0.015f * Heart_Size);
        if (transform.localScale.x >= 0.015f * Max)
        {
            Is_Max = true;
            if (plot != null)
            {
                plot_1.Close_Image();
                plot_2.Close_Image();
                plot.Open_Image();
                plot = null;
            }
        }
    }
    public void Open_Wall()
    {
        Cut_Time += Time.deltaTime;
        if (Cut_Time / Move_Time > 1.0f)
        {
            Destroy(transform.GetComponent<Heart>());
        }
        var Lerp_X = Mathf.Lerp(Wall.transform.position.x, Wall.transform.position.x + Move_Dircetion.x, Cut_Time / Move_Time);
        Wall.transform.position = new Vector3(Lerp_X, Wall.transform.position.y, Wall.transform.position.z);
    }

    public void Get_Heart()
    {
        Book_Heart.SetActive(true);
    }
}
