﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Check_Plane : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Move_Plane")
        {
            other.gameObject.layer = 22;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Move_Plane")
        {
            other.gameObject.layer = 18;
        }
    }
}
