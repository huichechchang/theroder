﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note_Book : MonoBehaviour
{
    [Header("筆記本內頁")]
    [SerializeField]
    private GameObject Stuff = null;
    [Header("啟動開發者模式")]
    [SerializeField]
    private GameObject Develpoer = null;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        else if (Input.GetKeyDown(KeyCode.Slash))
        {
            Develpoer.SetActive(!Develpoer.activeInHierarchy);
        }
    }
    public void Show_Or_Close()
    {
        Stuff.SetActive(!Stuff.activeInHierarchy);
    }

}
