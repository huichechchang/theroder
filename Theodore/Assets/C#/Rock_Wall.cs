﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock_Wall : MonoBehaviour
{
    [Header("抓取要被控制的物件")]
    [SerializeField]
    private GameObject Wall = null;
    [Header("抓取可被觸控的桿子物件")]
    [SerializeField]
    private GameObject Gan_chi;
    [Header("抓取要被控制物件的原位置")]
    [SerializeField]
    private Vector3 Wall_Pos;
    [Header("設定開啟到完成的時間")]
    [SerializeField]
    private float Open_Time = 0;
    [Header("設定要上升的高度")]
    [SerializeField]
    private float Up_Float = 0;
    [SerializeField]
    private bool Enemy_Push = false;
    public  bool Open = false;
    public float Start_Walk_time = 0;
    [SerializeField]
    public AudioSource[] Wall_Audio;

    [Header("劇情動畫")]
    [SerializeField]
    private Plot plot = null;

    [Header("提示框")]
    [SerializeField]
    private GameObject Tips = null;

    [Header("判定試管是否伸起")]
    [SerializeField]
    private bool Tube = false;

    private void Awake()
    {
        Wall = transform.parent.GetChild(0).gameObject;
        Gan_chi = transform.GetChild(0).gameObject;
        Wall_Pos = Wall.transform.position;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Open == true)
        {
            Open_Wall();
        }
    }
    public void Open_Wall()
    {
        if (plot != null && !Tube)
        {
            plot.Open_Image();
            plot = null;
        }
        Start_Walk_time += Time.deltaTime;
        if (Start_Walk_time / Open_Time > 1.0f && transform.tag != "Ct_Cage")
        {
            if (plot != null && Tube)
            {
                plot.Open_Image();
                plot = null;
            }
            Destroy(transform.GetComponent<Rock_Wall>());
        }
        else if (Start_Walk_time / Open_Time > 1.0f && transform.tag == "Ct_Cage")
        {
            Start_Walk_time = 0;
            Up_Float = -Up_Float;
            Wall_Pos = Wall.transform.position;
            Open = false;
            return;
        }
        var Lerp_Y = Mathf.Lerp(Wall_Pos.y, Wall_Pos.y + Up_Float, Start_Walk_time / Open_Time);
        var Lerp_Y_Gan = 0f;//Mathf.Lerp(1,-1, Start_Walk_time / (Open_Time/2));
        if (Up_Float > 0)
        {
            Lerp_Y_Gan = Mathf.Lerp(-1, 1, Start_Walk_time / Open_Time);
        }
        else if (Up_Float < 0)
        {
            Lerp_Y_Gan = Mathf.Lerp(1, -1, Start_Walk_time / Open_Time);
        }
        Wall.transform.position = new Vector3(Wall_Pos.x, Lerp_Y, Wall_Pos.z); 
        Gan_chi.transform.localPosition = new Vector3 (Gan_chi.transform.localPosition.x,Lerp_Y_Gan, Gan_chi.transform.localPosition.z);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (Enemy_Push == true && other.gameObject.tag == "Soldier")
        {
            Open = true;
        }
       else  if (other.gameObject.tag == "Player")
        {
            Tips.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && Input.GetKeyDown(KeyCode.E))
        {
            Open = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player" )
        {
            Tips.SetActive(false);
        }
    }
}
