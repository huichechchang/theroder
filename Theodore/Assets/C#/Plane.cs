﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour
{
    [SerializeField]
    private GameObject Player = null;
    [SerializeField]
    private GameObject Move_Plane = null;
    [SerializeField]
    private Vector3[] Pos = null;
    [SerializeField]
    private float start_Move_Time = 0;
    [SerializeField]
    private int Move_Time = 0;//指定時間走到指定地點
    [SerializeField]
    private int Move_flag = 0;
    [SerializeField]
    private bool Retrun = false;
    [SerializeField]
    private Vector3 Original_Pos = new Vector3(0, 0, 0);
    [SerializeField]
    private float Lerp_X = 0, Lerp_Y = 0;
    private void Awake()
    {
        //Move_Time = Random.Range(3, 6);
        Move_Plane = this.gameObject.transform.parent.gameObject;
        Pos = new Vector3[Move_Plane.transform.childCount];
        for (int i = 0; i <= Move_Plane.transform.childCount - 1; i++)
        {
            Pos[i] = Move_Plane.transform.GetChild(i).transform.position;
        }
    }
    void Start()
    {
        Original_Pos = transform.position;
        if (Move_Time == 0)
        {
            Move_Time = Random.Range(3, 6);
        }
    }
    void Update()
    {
        start_Move_Time += Time.deltaTime;
        if (start_Move_Time / Move_Time > 1.0f)
        {
            start_Move_Time = 0;
            if (Retrun == false)
            {
                Move_flag += 1;
            }
            else
            {
                Move_flag -= 1;
            }
            if (Move_flag == Move_Plane.transform.childCount - 2)
            {
                Retrun = true;
            }//走到底了
            else if (Move_flag == 0)
            {
                Retrun = false;
            }//走回來了
        }
        if (Retrun == false)
        {
            Lerp_X = Mathf.Lerp(Pos[Move_flag].x, Pos[Move_flag + 1].x, start_Move_Time / Move_Time);
            Lerp_Y = Mathf.Lerp(Pos[Move_flag].y, Pos[Move_flag + 1].y, start_Move_Time / Move_Time);
        }
        else
        {
            Lerp_X = Mathf.Lerp(Pos[Move_flag].x, Pos[Move_flag - 1].x, start_Move_Time / Move_Time);
            Lerp_Y = Mathf.Lerp(Pos[Move_flag].y, Pos[Move_flag - 1].y, start_Move_Time / Move_Time);
        }
        transform.position = new Vector3(Lerp_X, Lerp_Y, transform.position.z);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //當玩家位於平台上且保持不動___跟隨平台
            if (collision.gameObject.GetComponent<Animator>().GetFloat("Walking") == 0)
            {
                collision.transform.SetParent(transform);
                //物件圖層將與移動平台相同
                collision.gameObject.layer = 18;
            }
            //反之 玩家隨心所欲移動
            else
            {
                collision.transform.parent = null;
                //物件圖層變回原樣
                //collision.gameObject.layer = 8;
            }
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.parent = null;
            //物件圖層變回原樣_從移動平台圖層變為正常Normal圖層
            if (collision.gameObject.layer == 18)
            {
                collision.gameObject.layer = 8;
            }
        }
    }
}
