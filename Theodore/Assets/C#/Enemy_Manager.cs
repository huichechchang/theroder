﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Manager : MonoBehaviour
{
    [Header("抓取玩家物件")]
    [SerializeField]
    protected GameObject Player = null;
    [Header("抓取瓦爾特物件")]
    [SerializeField]
    protected GameObject Light = null;
    [Header("抓取敵人位置元件")]
    protected Transform Enemy_transform;
    [Header("抓取敵人原尺寸")]
    [SerializeField]
    public Vector3 Normal_Size = Vector3.zero;
    [Header("抓取碰撞器元件")]
    [SerializeField]
    protected Animator Anim;
    [Header("抓取剛體元件")]
    [SerializeField]
    protected Rigidbody Rb;
    [Header("決定最左與最右巡邏點位置")]
    [SerializeField]
    protected float Left_Pos_X, Right_Pos_X;
    [Header("決定移動速度")]
    [SerializeField]
    protected float Speed = 20f;
    [Header("檢測面向右走")]
    [SerializeField]
    protected bool Face_Right = false;
    [Header("決定跟蹤速度")]
    [SerializeField]
    protected float Follow_Speed = 0f;
    [Header("決定敵人視線範圍")]
    [SerializeField]
    protected float Enemy_Attack_Range = 0f;
    [Header("查看敵人是否在追擊中")]
    [SerializeField]
    protected bool Is_Follow = false;
    [Header("檢測敵人是否被抓住")]
    [SerializeField]
    public bool Be_Catch = false;
    [Header("檢測敵人是否在籠子內")]
    [SerializeField]
    public bool Is_Cage = false;
    [Header("抓取滑鼠物件")]
    [SerializeField]
    private Mouse_Cursor My_Cursor = null;
    [Header("抓住牠的籠子")]
    [SerializeField]
    private GameObject Cage = null;
    /*[Header("當瓦爾特啟動且鼠標碰到敵人時_更換鼠標圖案")]
    [SerializeField]
    protected Texture2D Cursor_Texture;*/

    public enum State { Walk, Run, Woring, Go_Back, BeCatch };
    public State state;

    //抓取玩家物件
    protected virtual void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    //抓取自身各元件以及左巡邏點與右巡邏點的位置
    protected virtual void Start()
    {
        Enemy_transform = GetComponent<Transform>();
        Normal_Size = Enemy_transform.localScale;
        Anim = GetComponent<Animator>();
        Rb = GetComponent<Rigidbody>();
        Left_Pos_X = transform.GetChild(0).gameObject.transform.position.x;
        Right_Pos_X = transform.GetChild(1).gameObject.transform.position.x;
        //Destroy(transform.GetChild(0).gameObject);
        //Destroy(transform.GetChild(1).gameObject);
    }
    //檢測目前狀態以及方向
    protected virtual void Update()
    {
        Enemy_State();
        Face_Dircetion();
    }
    protected void FixedUpdate()
    {
        //Movement();
    }
    //檢視狀態機
    protected void Enemy_State()
    {
        switch (state)
        {
            //巡邏狀態
            case (State.Walk):
                //巡邏
                Movement();
                //檢視視野範圍內是否有目標
                Enemy_Ray();
                break;
            //追逐狀態
            case (State.Run):
                //檢測是否為追逐狀態
                Is_Follow = true;
                //追逐
                Follow();
                break;
            //緊戒狀態
            case (State.Woring):
                Enemy_Ray();
                break;
            //回歸狀態
            case (State.Go_Back):
                break;
            //被抓住狀態
            case (State.BeCatch):
                break;
        }
    }
    //巡邏狀態
    protected void Movement()
    {
        Anim.SetBool("Walk", true);
        Rb.velocity = new Vector3(Speed * Time.fixedDeltaTime, Rb.velocity.y, Rb.velocity.z);
    }
    //跟蹤狀態
    protected void Follow()
    {
        //檢測是否玩家仍在偵查範圍
        if (Vector3.Distance(Player.transform.position, transform.position) >= Mathf.Abs(Enemy_Attack_Range) + 0.05f
                             |
                             Player.layer == 10
                             |
                             Player.layer == 16
                             |
                             Be_Catch
                             )
                           
        {
            //進入緊戒
            Worring();
        }
        //逐步移動要玩家位置
        else
        {
            Anim.SetBool("Run", true);
            transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, Follow_Speed * Time.deltaTime);
        }
    }
    //敵人攻擊範圍
    protected void Enemy_Ray()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position - new Vector3(0, 0.015f, 0), new Vector3(Enemy_Attack_Range, 0, 0));
        Debug.DrawRay(transform.position - new Vector3(0, 0.015f, 0), new Vector3(Enemy_Attack_Range, 0, 0), Color.red);
        if (Physics.Raycast(ray, out hit, Mathf.Abs(Enemy_Attack_Range), LayerMask.GetMask("Normal")))
        {
            if (hit.transform.gameObject.tag == "Player")
            {
                state = State.Run;
            }
        }
    }
    //確認方向
    protected void Face_Dircetion()
    {
        switch (state)
        {
            //巡邏狀態
            case (State.Walk):
                //如果敵人超出左巡邏點//轉回右邊
                if (transform.position.x < Left_Pos_X)
                {
                    FaceRight();
                }
                //如果敵人超出右巡邏點//轉回左邊
                else if (transform.position.x > Right_Pos_X)
                {
                    FaceLeft();
                }
                break;
            //追逐狀態
            case (State.Run):
                //如果要追逐的玩家位於本敵人右方
                if (transform.position.x < Player.transform.position.x)
                {
                    //轉向右邊
                    FaceRight();
                }
                //如果要追逐的玩家位於本敵人左方
                else if (transform.position.x > Player.transform.position.x)
                {
                    //轉向左邊
                    FaceLeft();
                }
                break;
            case (State.BeCatch):
                break;
                //緊戒狀態
        }
    }
    //面向右邊
    protected void FaceRight()
    {
        Enemy_transform.localScale = new Vector3(Mathf.Abs(Enemy_transform.localScale.x), Enemy_transform.localScale.y, Enemy_transform.localScale.z);
        Face_Right = true;
        Speed = Mathf.Abs(Speed);
        Enemy_Attack_Range = Mathf.Abs(Enemy_Attack_Range);
    }
    //面向左邊
    protected void FaceLeft()
    {
        Enemy_transform.localScale = new Vector3(-Mathf.Abs(Enemy_transform.localScale.x), Enemy_transform.localScale.y, Enemy_transform.localScale.z);
        Face_Right = false;
        Speed = -Mathf.Abs(Speed);
        Enemy_Attack_Range = -Mathf.Abs(Enemy_Attack_Range);
    }
    //進入警戒狀態
    public void Worring()
    {
        Anim.SetBool("Run", false);
        Anim.SetTrigger("Worring");
        state = State.Woring;
    }
    //警戒狀態結束
    protected void Worring_End()
    {
        state = State.Walk;
    }
    //改變敵人自身大小
    public void Big_Or_Smell(float Min, float Max, float Size)
    {
        var Enemy_Size = Mathf.Lerp(Min, Max, Size);
        if (transform.localScale.x > 0)
        {
            transform.localScale = new Vector2(Mathf.Abs(0.01f * Enemy_Size), 0.01f * Enemy_Size);
        }
        else if (transform.localScale.x < 0)
        {
            transform.localScale = new Vector2(-Mathf.Abs(0.01f * Enemy_Size), 0.01f * Enemy_Size);
        }
    }
    //回復原尺寸
    public void Retrun_Normal_Size()
    {
        Debug.Log("喚回元此吋");
        Be_Catch = false;
        Worring();
        //標記這個敵人被抓住了
        if (transform.localScale.x > 0)
        {
            transform.localScale = new Vector2(Mathf.Abs(Normal_Size.x), Normal_Size.y);
        }
        else if (transform.localScale.x < 0)
        {
            transform.localScale = new Vector2(-Mathf.Abs(Normal_Size.x), Normal_Size.y);
        }
    }
    //敵人本身被推動
    public void Push(float Push_Force)
    {
        transform.GetComponent<Rigidbody>().velocity = new Vector3(Push_Force, 0, 0);
    }
    //敵人被抓住
    public void Be_Catching()
    {
        state = State.BeCatch;
        Be_Catch = true;
        Anim.SetBool("Run", false);
        Anim.SetBool("Catch", true);
        Anim.SetTrigger("Is_Catch");
    }
    //結束被抓住
    public void End_Catch()
    {
        Be_Catch = false;
        Anim.SetBool("Catch", false);
    }

    protected void OnTriggerEnter(Collider other)
    {
       /* if (other.gameObject.tag == "Cage" && !Is_Cage)
        {
            Is_Cage = true;
            var Kg_Mess = GameObject.FindGameObjectWithTag("Kg_Mess");
            Kg_Mess.GetComponent<Kg_Mess_2>().Enemy_Int += 1;
          
        }*/
    }
    private void OnTriggerExit(Collider other)
    {
     /*   if (other.gameObject.tag == "Cage" && Is_Cage)
        {
            Is_Cage = false;
            var Kg_Mess = GameObject.FindGameObjectWithTag("Kg_Mess");
            Kg_Mess.GetComponent<Kg_Mess_2>().Enemy_Int -= 1;
            //變化圖層_角色不會去撞到被關著的敵人
            //gameObject.layer = 15;

        }*/
    }
    private void OnCollisionStay(Collision collision)
    {
        //給予向上的力，讓角色可以爬樓梯
        if (collision.gameObject.tag == "Down")
        {
            var Stairs_GameObject = collision.gameObject;
            switch (Stairs_GameObject.name)
            {
                case ("Left_Stairs"):
                    if (!Face_Right)
                    {
                        Rb.AddForce(Vector3.up * 4.5f * Rb.mass);
                    }
                    break;
                case ("Right_Stairs"):
                    if (Face_Right)
                    {
                        Rb.AddForce(Vector3.up * 4.5f * Rb.mass);
                    }
                    break;
            }
        }
       /* if (collision.gameObject.tag == "The_Cage" && !Is_Cage)
        {
            Debug.Log("關起來了");
            Is_Cage = true;
            var Kg_Mess = GameObject.FindGameObjectWithTag("Kg_Mess");
            Kg_Mess.GetComponent<Kg_Mess_2>().Enemy_Int += 1;
            //切換到被關起來的圖層
            gameObject.layer = 24;
        }*/
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Kg")
        {
            if (collision.gameObject.transform.parent.GetComponent<Kg_Mess_2>() != null)
            {
                collision.gameObject.transform.parent.GetComponent<Kg_Mess_2>().Mess += 1;
            }
        }
        if (collision.gameObject.tag == "The_Cage" && !Is_Cage)
        {
            Debug.Log("關起來了");
            Cage = collision.gameObject;
            Is_Cage = true;
            var Kg_Mess = GameObject.FindGameObjectWithTag("Kg_Mess");
            Kg_Mess.GetComponent<Kg_Mess_2>().Enemy_Int += 1;
            //切換到被關起來的圖層
            gameObject.layer = 24;
            //籠子切換圖層，避免2次觸發
            Cage.layer = 24;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Kg")
        {
            if (collision.gameObject.transform.parent.GetComponent<Kg_Mess_2>() != null)
            {
                collision.gameObject.transform.parent.GetComponent<Kg_Mess_2>().Mess -= 1;
            }
        }
        if (collision.gameObject.tag == "Cage" && gameObject.layer==24)
        {
            Debug.Log("放出來了");
            if (Cage != null)
            {
                //籠子切換圖層，換回來
                Cage.layer = 25;
            }
            Is_Cage = false;
            var Kg_Mess = GameObject.FindGameObjectWithTag("Kg_Mess");
            Kg_Mess.GetComponent<Kg_Mess_2>().Enemy_Int -= 1;
            //切換到被關起來的圖層
            gameObject.layer = 9;
        }
    }

    private void OnMouseEnter()
    {
        //My_Cursor.Start_Lock();
        if (transform.tag == "Gem")
        {
            My_Cursor.Start_Lock();
        }

    }
    private void OnMouseExit()
    {
        //My_Cursor.End_Lock();
        if (transform.tag == "Gem")
        {
             My_Cursor.End_Lock();
        }
    }

}
