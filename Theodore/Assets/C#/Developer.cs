﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Developer : MonoBehaviour
{
    [Header("存檔點")]
    [SerializeField]
    private GameObject Place_0, Place_1;
    [Header("抓取玩家物件")]
    [SerializeField]
    private GameObject Player;
    private void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Player.transform.position = Place_0.transform.position;
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            Player.transform.position = Place_1.transform.position;
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {
            Player.GetComponent<Player_Controller>().Developer_Power();
        }
    }
}
