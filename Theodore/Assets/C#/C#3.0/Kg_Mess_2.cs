﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Kg_Mess_2 : MonoBehaviour
{
    [Header("抓取在體重機上敵人的數量")]
    [SerializeField]
    public float Mess = 0;
    [Header("抓取在本體上物體們的碰撞器")]
    [SerializeField]
    private Collider[] Cast_Enemy;
    [Header("抓取在本體上物體們的碰撞器")]
    [SerializeField]
    private GameObject[] Enemys;
    [Header("抓取籠子物件")]
    [SerializeField]
    private GameObject Cage = null;
    [Header("抓取石牆物件_通往1_2")]
    [SerializeField]
    private GameObject Wall = null;
    [Header("抓取石牆物件位置_通往1_2")]
    [SerializeField]
    private Vector3 Wall_Pos = Vector3.zero;
    [Header("控制石牆要往哪移動")]
    [SerializeField]
    private float Move_Dircetion = 0f;
    [Header("控制石牆移動時間")]
    [SerializeField]
    private float Move_Time = 0f;
    [Header("移動計時")]
    [SerializeField]
    private float Cut_Time = 0f;
    [Header("判斷本體上的敵人數目是否達標")]
    [SerializeField]
    private bool Three_Enemy = false;
    [SerializeField]
    private bool Draw = false;
    [SerializeField]
    private GameObject[] Show;
    [Header("原本的圖片")]
    [SerializeField]
    private Sprite Normal_Sprite = null;
    [Header("感應後切換的圖片")]
    [SerializeField]
    private Sprite Change_Sprite = null;
    [Header("獲得的心臟碎片")]
    [SerializeField]
    private Image Heart_Image;
    [Header("關起來的敵人數量")]
    [SerializeField]
    public int Enemy_Int = 0;

    [Header("提示前進箭頭")]
    [SerializeField]
    private GameObject Going = null;

    [Header("書本亮光")]
    [SerializeField]
    private GameObject Bool_Light = null;

    [Header("過場動畫3位置")]
    [SerializeField]
    private GameObject OverView_3 = null;

    [Header("關住兩隻寶石人後動畫")]
    [SerializeField]
    private Plot plot = null;

    public GameObject Light_1;
    public GameObject Light_2;
    public GameObject Light_3;

    void Start()
    {
        Wall_Pos = Wall.transform.position;
        Show = new GameObject[3];
        Draw = true;
        for (int i = 0; i <= 2; i++)
        {
            Show[i] = transform.GetChild(i).gameObject;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Three_Enemy)
        {
            Check_Enemy();
        }
        else
        {
            //得到碎片
            Get_Heart();
            //開啟石牆
            Open_Wall();
        }
    }
    private void Check_Enemy()
    {
        //Mess = 0;
        /* Cast_Enemy = Physics.OverlapBox(transform.position, transform.localScale / 2, Quaternion.identity, LayerMask.GetMask("Enemy"));
         for (int i = 0; i <= Cast_Enemy.Length - 1; i++)
         {
             //Mess += Cast_Enemy[i].gameObject.GetComponent<Rigidbody>().mass;
             Enemys = new GameObject[Cast_Enemy.Length];
             Enemys[i] = Cast_Enemy[i].gameObject;
         }*/
        switch (Mess)
        {
            case (0):
                for (int i = 0; i <= Show.Length - 1; i++)
                {
                    Show[i].GetComponent<SpriteRenderer>().sprite = Normal_Sprite;
                    Light_1.SetActive(false);
                    Light_2.SetActive(false);
                    Light_3.SetActive(false);
                }
                return;
            case (1):
                Show[0].GetComponent<SpriteRenderer>().sprite = Change_Sprite;
                Light_1.SetActive(true);
                Show[1].GetComponent<SpriteRenderer>().sprite = Normal_Sprite;
                Light_2.SetActive(false);
                Show[2].GetComponent<SpriteRenderer>().sprite = Normal_Sprite;
                Light_3.SetActive(false);
                break;
            case (2):
                Show[0].GetComponent<SpriteRenderer>().sprite = Change_Sprite;
                Light_1.SetActive(true);
                Show[1].GetComponent<SpriteRenderer>().sprite = Change_Sprite;
                Light_2.SetActive(true);
                Show[2].GetComponent<SpriteRenderer>().sprite = Normal_Sprite;
                Light_3.SetActive(false);
                break;
            case (3):
                Show[0].GetComponent<SpriteRenderer>().sprite = Change_Sprite;
                Light_1.SetActive(true);
                Show[1].GetComponent<SpriteRenderer>().sprite = Change_Sprite;
                Light_2.SetActive(true);
                Show[2].GetComponent<SpriteRenderer>().sprite = Change_Sprite;
                Light_3.SetActive(true);
                break;
        }
        if (Enemy_Int == 2)
        {
            /*if (plot != null)
            {
                plot.Open_Image();
                plot = null;
            }*/
            Three_Enemy = true;
        }
        /*if (Enemy_Int == 3)
        {
            Three_Enemy = true;
        }*/
    }
    //放下籠子
    public void Open_Cage()
    {
        Cage.GetComponent<Rigidbody>().useGravity = true;
    }
    public void Get_Heart()
    {
        Three_Enemy = false;
        Enemy_Int = 0;
        Debug.Log("得到碎片");
        Heart_Image.gameObject.SetActive(true);
        Going.SetActive(true);
        Bool_Light.SetActive(true);
        OverView_3.SetActive(true);
    }
    //拉開石牆_開啟1-2
    public void Open_Wall()
    {
        Cut_Time += Time.deltaTime;
        if (Cut_Time / Move_Time > 1.0f)
        {
            Destroy(transform.GetComponent<Kg_Mess_2>());
        }
        var Lerp_Y = Mathf.Lerp(Wall_Pos.y, Wall_Pos.y + Move_Dircetion, Cut_Time / Move_Time);
        Wall.transform.position = new Vector3(Wall.transform.position.x, Lerp_Y, Wall.transform.position.z);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (Draw == true)
        {
            Gizmos.DrawWireCube(transform.position, transform.localScale);
        }
    }
}