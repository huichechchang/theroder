﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverView : MonoBehaviour
{
    [Header("遊戲結束畫面")]
    [SerializeField]
    private GameObject End_Image = null;

    [Header("劇情插入")]
    [SerializeField]
    private Plot plot = null;
    void Start()
    {
        //Time.timeScale = 0;
    }
    private void Awake()
    {
        Time.timeScale = 0;
    }
    public void End()
    {
        if (plot == null)
        {
            Time.timeScale = 1;
            transform.gameObject.SetActive(false);
        }
        else
        {
            plot.Open_Image();
            transform.gameObject.SetActive(false);
        }
    }
    public void End_UI()
    {
        if (End_Image != null)
        {
            End_Image.SetActive(true);
        }
    }
}
